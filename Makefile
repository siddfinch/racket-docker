DOCKER=racket
user=$(DOCKER)

default:
	@echo $(DOCKER)-build -- build docker image $(DOCKER)
	@echo $(DOCKER)-run  -- run docker image $(DOCKER)

.PHONY: build run
build:
	@echo build $(DOCKER)
	docker build -t $(DOCKER) -f Dockerfile .
run: build
	@echo run $(DOCKER)
	docker run --rm -it $(DOCKER) /bin/bash
