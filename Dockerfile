FROM centos:latest
RUN [ "yum", "install", "epel-release", "-y" ]
RUN yum update -y
RUN yum install -y curl
RUN yum install -y ed
RUN yum install -y libedit readline
RUN curl -t fsSL https://mirror.racket-lang.org/installers/7.4/racket-7.4-x86_64-linux.sh > /root/racket.sh  && echo "yes\n1\n" | /bin/bash /root/racket.sh  && rm /root/racket.sh
RUN adduser -m racket
USER racket
WORKDIR /home/racket
