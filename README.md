# Dockerfile for Racket

A dockerfile for Racket (http://www.racket-lang.org)

```
$ make
racket-build -- build docker image racket
racket-run -- run docker image racket
```